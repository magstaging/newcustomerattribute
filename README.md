1. clone the repository

2. create folder app/code/Mbs/NewCustomerAttribute when located at the root of the Magento site

3. copy the content of this repository within the folder

4. install the module php bin/magento setup:upgrade

5. verify both new customer attributes are available in Magento backend form and the data can be saved
